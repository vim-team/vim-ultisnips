Source: vim-ultisnips
Maintainer: Nicholas Guriev <guriev-ns@ya.ru>
Section: editors
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               dh-sequence-vim-addon,
               python3-all
Standards-Version: 4.6.1
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/vim-team/vim-ultisnips
Vcs-Git: https://salsa.debian.org/vim-team/vim-ultisnips.git
Homepage: https://github.com/SirVer/ultisnips

Package: vim-ultisnips
Architecture: all
Depends: vim-nox | vim-python3,
         ${misc:Depends},
         ${python3:Depends},
         ${vim-addon:Depends}
Recommends: vim-snippets
Description: snippet solution for Vim
 UltiSnips provides snippet management for the Vim editor. A snippet is a short
 piece of text that is either re-used often or contains a lot of redundant
 text. UltiSnips allows you to insert a snippet with only a few key strokes.
 Snippets are common in structured text like source code but can also be used
 for general editing like, for example, inserting a signature in an email or
 inserting the current date in a text file.
 .
 This plugin can utilize the snippets provided by the vim-snippets package.
